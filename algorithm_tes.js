function countUnique(nilai, n) {
  let hasil = 1;

  for (let i = 1; i < n; i++) {
    let j = 0;
    for (j = 0; j < i; j++) if (nilai[i] === nilai[j]) break;

    if (i === j) hasil++;
  }
  return hasil;
}

// let nilai = [1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13];
let nilai = [1, 2, 2, 2, 2, 2, 2, 2, 4, 6];
let n = nilai.length;
console.log(countUnique(nilai, n));
